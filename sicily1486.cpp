//题目分析:
//没学过C的电子通工自动化啊。
//这题后我才知道scanf和printf和cin和cout的时间的差距啊。。。
//都是通过阅读各校友的代码才知道怎么破。
//本来也没打算用快排，想构造双向有序链表去解决，但是搜索太耗时间了，
//代码量也大，所以~~~就这样吧。

//题目网址:http://soj.me/1486

#include <iostream>
#include <stdio.h>
#include <algorithm>

using namespace std;

int number[200001] = {0};

int main()
{
    int howMany;
    int num;
    int count;
    bool flag = false;

    while (scanf("%d", &howMany) != EOF) {

        if (flag)
            printf("\n");
        flag = true;

        for (int i = 0; i < howMany; i++) {
            scanf("%d", &number[i]);
        }

        sort(number, number + howMany);

        num = number[0];
        count = 1;
        for (int i = 1; i < howMany; i++) {
            if (num != number[i]) {
                printf("%d %d\n", num, count);
                num = number[i];
                count = 1;
            } else {
                count++;
            }
        }
        printf("%d %d\n", num, count);
    }

    return 0;
}